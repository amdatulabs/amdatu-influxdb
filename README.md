# README #

Amdatu Influxdb is a small OSGi wrapper for [influxdb-java](https://github.com/influxdb/influxdb-java). It provides a ManagedServiceFactory that creates services of type org.influxdb.InfluxDB, which gives access to the API of influxdb-java. Amdatu Influxdb wraps most of influxdb-java's dependencies and exports it's API to make it easy to use in an OSGi application.