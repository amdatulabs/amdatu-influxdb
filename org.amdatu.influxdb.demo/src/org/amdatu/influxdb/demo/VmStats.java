package org.amdatu.influxdb.demo;


import java.lang.management.ManagementFactory;
import java.util.concurrent.TimeUnit;

import org.amdatu.scheduling.annotations.RepeatForever;
import org.amdatu.scheduling.annotations.RepeatInterval;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.influxdb.InfluxDB;
import org.influxdb.dto.Point;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

@Component
@RepeatForever
@RepeatInterval(period=RepeatInterval.SECOND, value=1)
public class VmStats implements Job{

	private static final String USED = "used";
	private static final String COMMITED = "commited";
	private static final String MAX = "max";
	
	@ServiceDependency
	private volatile InfluxDB m_influxdb;
	
	@Start
	public void start() {
		m_influxdb.createDatabase("amdatusample");
		m_influxdb.enableBatch(2000, 1000, TimeUnit.MILLISECONDS);
	}
	
	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		java.lang.management.MemoryUsage memoryUsage = ManagementFactory.getMemoryMXBean().getHeapMemoryUsage();
		
		Point point1 = Point.measurement("memory")
		                    .time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
		                    .field(USED, Long.toString(memoryUsage.getUsed()))
		                    .field(COMMITED, Long.toString(memoryUsage.getCommitted()))
		                    .field(MAX, Long.toString(memoryUsage.getMax()))
		                    .build();

		m_influxdb.write("amdatusample", "default", point1);
	}

}
