package org.amdatu.influxdb.demo;

import java.util.ArrayList;
import java.util.List;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;

@Component(properties={ 
		@Property(name="osgi.command.scope", value="demo"),
		@Property(name="osgi.command.function", value={"leak", "free"})
}, provides=Object.class)
public class Leaker {

	private final List<byte[]> bytes = new ArrayList<>();

	//Leak 100 mb
	public void leak() {

		bytes.add(new byte[(int) 1e+8]);
	}
	
	//Free allocated bytes and force a gc
	public void free() {
		bytes.clear();
		System.gc();
		System.gc();
	}
}
