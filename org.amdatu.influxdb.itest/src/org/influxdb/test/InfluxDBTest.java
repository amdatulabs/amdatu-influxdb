/*
 * Copyright (c) 2016 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.influxdb.test;

import static org.amdatu.testing.configurator.TestConfigurator.*;
import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import org.influxdb.*;
import org.influxdb.dto.*;

public class InfluxDBTest {

		private volatile InfluxDB m_influxDb;
	
	@Before
	public void setup() throws InterruptedException {
				configure(this)
			.add(createFactoryConfiguration("org.amdatu.influxdb").set("url", "http://localhost:8086").set("username", "root").set("password", "root"))
			.add(createServiceDependency().setService(InfluxDB.class).setRequired(true))
			.apply();
				
		}
	
	@Test
	public void test() {
		m_influxDb.createDatabase("testdb");
		
		BatchPoints batchPoints = BatchPoints.database("testdb").tag("async", "true").retentionPolicy("default")
				.consistency(InfluxDB.ConsistencyLevel.ALL).build();
		Point point1 = Point.measurement("cpu").time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
				.field("idle", 90L).field("system", 9L).field("system", 1L).build();
		Point point2 = Point.measurement("disk").time(System.currentTimeMillis(), TimeUnit.MILLISECONDS)
				.field("used", 80L).field("free", 1L).build();
		batchPoints.point(point1);
		batchPoints.point(point2);
		m_influxDb.write(batchPoints);
		Query query = new Query("SELECT idle FROM cpu", "testdb");
		QueryResult result = m_influxDb.query(query);
		
		assertEquals(1, result.getResults().size());
	}		
	
	@After
	public void after() {
		cleanUp(this);
		
		m_influxDb.deleteDatabase("testdb");
	}
}
