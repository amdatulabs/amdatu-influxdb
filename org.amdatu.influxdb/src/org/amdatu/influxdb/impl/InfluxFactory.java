/*
 * Copyright (c) 2016 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.influxdb.impl;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.amdatu.influxdb.InfluxdbRegistry;
import org.apache.felix.dm.DependencyManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.osgi.framework.Constants;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedServiceFactory;
import org.osgi.service.log.LogService;

@Component(properties=@Property(name=Constants.SERVICE_PID, value="org.amdatu.influxdb"))
public class InfluxFactory implements ManagedServiceFactory, InfluxdbRegistry {
	private final Map<String, org.apache.felix.dm.Component> m_components = new ConcurrentHashMap<>();
	
	@Inject
	private volatile DependencyManager m_dependencyManager;
	
	@ServiceDependency
	private volatile LogService m_logService;

	@Override
	public String getName() {
		return "org.amdatu.influxdb";
	}

	@Override
	public void updated(String pid, Dictionary<String, ?> properties) throws ConfigurationException {
		if(!m_components.containsKey(pid)) {
			
			String url = getRequiredProperty(properties, "url");
			String username = getRequiredProperty(properties, "username");
			String password = getRequiredProperty(properties, "password");
			
			Properties serviceProperties = new Properties();
			serviceProperties.put("url", url);
			InfluxDB influxDB = InfluxDBFactory.connect(url, username, password);
			
			try {
				org.apache.felix.dm.Component component = m_dependencyManager.createComponent().setInterface(InfluxDB.class.getName(), serviceProperties).setImplementation(influxDB);
				m_dependencyManager.add(component);
				m_components.put(pid, component);
				m_logService.log(LogService.LOG_INFO, "InfluxDB for url '" + url + "' is registered");
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		
		}
	}

	private String getRequiredProperty(Dictionary<String, ?> properties, String propertyName) throws ConfigurationException {
		String prop = (String)properties.get(propertyName);
		if(prop == null) {
			throw new ConfigurationException(propertyName, "Required property " + propertyName + " not set");
		}
		return prop;
	}

	@Override
	public void deleted(String pid) {
		org.apache.felix.dm.Component component = m_components.remove(pid);
		m_dependencyManager.remove(component);
	}

	@Override
	public Map<String, Map<String, Object>> listConfig() {
		
		Map<String, Map<String, Object>> config = new HashMap<>();
		m_components.entrySet().forEach(e -> config.put(e.getKey(), dictionaryToMap(e.getValue().getServiceProperties())));
		
		return config;
	}
	
	private Map<String, Object> dictionaryToMap(Dictionary<Object, Object> dict) {
		Map<String, Object> result = new HashMap<>();
		
		Enumeration<Object> keys = dict.keys();
		while(keys.hasMoreElements()) {
			Object key = keys.nextElement();
			result.put(key.toString(), dict.get(key));
		}
		
		return result;
		
	}
}
