package org.amdatu.influxdb;

import java.util.Map;

public interface InfluxdbRegistry {
	Map<String, Map<String, Object>> listConfig();
}
