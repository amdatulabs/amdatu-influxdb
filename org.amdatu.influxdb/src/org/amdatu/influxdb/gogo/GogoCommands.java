package org.amdatu.influxdb.gogo;

import java.util.Map;

import org.amdatu.influxdb.InfluxdbRegistry;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component(properties={ 
		@Property(name="osgi.command.scope", value="influxdb"),
		@Property(name="osgi.command.function", value="status")
}, provides=Object.class)
public class GogoCommands {

	@ServiceDependency
	private volatile InfluxdbRegistry m_registry;
		
	public void status() {
		Map<String, Map<String, Object>> listConfig = m_registry.listConfig();
		if(listConfig.isEmpty()) {
			System.out.println("No influxdb configured!");
		} else {
			StringBuffer b = new StringBuffer("Infludb configurations\n==============================\n\n");
			
			listConfig.entrySet().forEach(e -> {
				b.append(e.getKey()).append("\n");
				e.getValue().entrySet().forEach(property -> {
					b.append("- ").append(property.getKey()).append(": ").append(property.getValue()).append("\n");
				});
				
				System.out.println(b.append("\n"));
			});
		}
	}
}
